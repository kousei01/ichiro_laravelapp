# ichiro_laravelApp

Ichiro ITS Laravel Web App ***(udah almarhum)***

## Requirements
Create `symlinkcreate.php` to symlink storage file in `lsapp_ichi` folder to `public_html` folder in home

```php
<?php
symlink('/home2/ichiroits/lsapp_ichi/storage/app/public', '/home2/ichiroits/public_html/storage');
?>
```
Run the `symlinkcreate.php` on your browser then delete it.

Make sure that .htaccess file in `public_html` contains this code:
```apache
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
```