<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/hello', function () {
    // return view('welcome');
    return '<h1>Hello World</h1>';
});

Route::get('/users/{id}/{name}', function($id, $name){
    return 'This is user '.$name.' with an id of '.$id;
});

Route::get('/about', function(){
    return view('pages.about');
});

*/

Route::get('/about', 'PagesController@about');
Route::get('/achievements', 'PagesController@achievements');
Route::get('/admin', 'PagesController@admin');
Route::get('/gallery', 'PagesController@gallery');
Route::get('/', 'PagesController@index');
// Route::get('/news', 'PagesController@news');
Route::get('/oprec', 'PagesController@oprec');
Route::get('/robots', 'PagesController@robots');
Route::get('/team', 'PagesController@team');

Route::resource('news', 'PostsController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
