<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use DB;


class PagesController extends Controller
{
    public function index(){
        $title = 'Welcome Home!';
        // return view('pages.index', compact('title'));
        $posts = Post::orderBy('created_at', 'desc')->paginate(3);
        return view('pages.index')->with('posts', $posts);
    }

    public function about(){
        $title = 'This is About Page';
        return view('pages.about')->with('title', $title);
    }

    public function achievements(){
        $title = 'This is Achievements Page';
        return view('pages.achievements')->with('title', $title);
    }

    public function admin(){
        $title = 'This is Admin Page';
        return view('pages.admin')->with('title', $title);
    }

    public function gallery(){
        $title = 'This is Gallery Page';
        return view('pages.gallery')->with('title', $title);
    }

    public function news(){
        $title = 'This is News Page';
        return view('pages.news')->with('title', $title);
    }

    public function oprec(){
        $title = 'This is Oprec Page';
        return view('pages.oprec')->with('title', $title);
    }

    public function robots(){
        $title = 'This is Robots Page';
        return view('pages.robots')->with('title', $title);
    }

    public function team(){
        $data = array(
            'title' => 'TEAM',
            'divisions' => ['Programming', 'Mechanic', 'Electronic', 'Official']

        );
        return view('pages.team')->with($data);
    }
}
