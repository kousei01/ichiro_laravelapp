{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        @include('inc.navbar')
        <a href="/news" class="btn btn-default">Go Back</a>
        <h1>{{$post->title}}</h1>
        <div>
            {{$post->body}}
        </div>
        <hr>
        <small>Written on {{$post->created_at}}</small>
    </body>
</html> --}}

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="{{asset('assets/images/ichiro.png')}}" type="image/x-icon">
        <meta name="description" content="">

        <title>News - Ichiro ITS</title>

        <link rel="stylesheet" href="{{asset('assets/tether/tether.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap-grid.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap-reboot.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/dropdown/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/socicon/css/styles.css')}}">
        <link rel="stylesheet" href="{{asset('assets/theme/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/mobirise/css/mbr-additional.css')}}" type="text/css">
    </head>

    <body>
        <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                        <a href="/">
                            <img src="{{asset('assets/images/white-text-169x60.png')}}" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                        </a>
                    </span>

                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                            </a></li>
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                            Achievements</a></li>
                        <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">OPREC</a></li> -->
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">Robots</a></li>
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">Gallery</a></li>
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="/about">About Us</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>

        <section class="mbr-section content5 cid-rnZHvLz4HW mbr-parallax-background" id="content5-p">
            <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(35, 35, 35);">
            </div>
            <div class="container">
                <div class="media-container-row">
                    <div class="title col-12 col-md-8">
                        <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">News</h2>
                    </div>
                </div>
            </div>
        </section>

        {{-- news--}}
        <section class="mbr-section content7 cid-rnZJ427bsK" id="content7-v">
            <div class="container">
                <div class="media-container-row">
                    <div class="col-12 col-md-12">
                        <div class="media-container-row">
                            <div class="media-content">
                                <div class="mbr-section-text">
                                    <p class="mbr-text align-right mb-0 mbr-fonts-style display-7"></p>
                                    <h1><strong><br><br>{{$post->title}}</strong></h1><br>
                                    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small> <br><br>
                                    <img style="width:50%" src="/storage/cover_images/{{$post->cover_image}}" alt="">
                                    <br><br><br>{!!$post->body!!}
                                </div>

                                @if(!Auth::guest())
                                    <br><br><br><br>
                                    <a href="/news/{{$post->id}}/edit" class="btn btn-warning display-4">EDIT</a>
                                    {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('Delete', ['class' => 'btn btn-primary display-4'])}}
                                    {!!Form::close() !!}
                                @endif
                            </div>
                            {{-- <div class="mbr-figure" style="width: 100%;">
                                <img src="{{asset('assets/images/fira3-864x648.jpg')}}" alt="Mobirise" title="">
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- @if(count($posts) > 1)
            @foreach ($posts as $post)
                <section class="mbr-section content7 cid-rnZN6wmxrZ">
                    <div class="container">
                        <div class="media-container-row">
                            <div class="col-12 col-md-12">
                                <div class="media-container-row">
                                    <div class="media-content">
                                        <div class="mbr-section-text">
                                            <h3><strong><br><br><a href="/news/{{$post->id}}">{{$post->title}}</a></strong></h3>
                                            <small>Written on {{$post->created_at}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endforeach
        @else
            <p>No Post Found</p>
        @endif --}}

        <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">
            <div class="container">
                <div class="media-container-row content mbr-white">
                    <div class="col-12 col-md-3 mbr-fonts-style display-7">
                        <p class="mbr-text">
                            <strong>Address</strong>
                            <br>
                            <br>Gedung Robotika ITS
                            <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                            <br>
                            <br><strong>Contacts</strong>
                            <br>
                            <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                    </div>
                    <div class="col-12 col-md-3 mbr-fonts-style display-7">
                        <p class="mbr-text">
                            <strong>Links</strong>
                            <br>
                            <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                            <br><strong>Feedback</strong>
                            <br>
                            <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                                allowfullscreen=""></iframe></div>
                    </div>
                </div>
                <div class="footer-lower">
                    <div class="media-container-row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="media-container-row mbr-white">
                        <div class="col-sm-6 copyright">
                            <p class="mbr-text mbr-fonts-style display-7">
                                © Copyright 2019 Ichiro ITS - All Rights Reserved
                            </p>
                        </div>
                        <div class="col-md-6">
                            <div class="social-list align-right">
                                <!-- <div class="soc-item">
                                    <a href="https://twitter.com" target="_blank">
                                        <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div>
                                <div class="soc-item">
                                    <a href="https://www.facebook.com" target="_blank">
                                        <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div> -->
                                <div class="soc-item">
                                    <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                        <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div>
                                <div class="soc-item">
                                    <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                        <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div>
                                <!-- <div class="soc-item">
                                    <a href="https://plus.google.com" target="_blank">
                                        <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div>
                                <div class="soc-item">
                                    <a href="https://www.behance.net" target="_blank">
                                        <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                    </a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>        

        <script src="{{asset('assets/web/assets/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/popper/popper.min.js')}}"></script>
        <script src="{{asset('assets/tether/tether.min.js')}}"></script>
        <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/smoothscroll/smooth-scroll.js')}}"></script>
        <script src="{{asset('assets/dropdown/js/script.min.js')}}"></script>
        <script src="{{asset('assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
        <script src="{{asset('assets/parallax/jarallax.min.js')}}"></script>
        <script src="{{asset('assets/theme/js/script.js')}}"></script>


        <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
    </body>

</html>