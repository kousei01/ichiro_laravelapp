{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Gallery - Ichiro ITS</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/gallery/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">



        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        History</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="carousel slide cid-ro4n4iww7p" data-interval="false" id="slider1-1s">



        <div class="full-screen">
            <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="6000">
                <ol class="carousel-indicators">
                    <li data-app-prevent-settings="" data-target="#slider1-1s" data-slide-to="0"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-1s" data-slide-to="1"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-1s" class=" active" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(assets/images/img-20190213-wa0000-1280x853.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div><img src="assets/images/img-20190213-wa0000-1280x853.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-center">
                                        <h2 class="mbr-fonts-style display-1">Gallery</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">We captured every precious moment<br>Because every captured moments are always more than just a picture</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="https://www.youtube.com/watch?v=tkXSHAZW3Xg">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div><img src="assets/images/robot2.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left">
                                        <h2 class="mbr-fonts-style display-1">2018 : AFTERMOVIE</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">A short movie about our journey through 2018</p>
                                        <div class="mbr-section-btn" buttons="0">
                                            <!-- <a class="btn btn-primary display-4" href="#">Learn More</a> -->
                                            <a class="btn  btn-white-outline display-4" href="https://www.youtube.com/watch?v=tkXSHAZW3Xg" target="_blank">WATCH THE FULL VERSION</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url(assets/images/robot2.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div><img src="assets/images/robot2.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-right">
                                        <h2 class="mbr-fonts-style display-1">GALLERY</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">We captured every precious moment
                                            <br>Because every captured moments are always more than just a picture</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-1s"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a>
                <a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-1s"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a>
            </div>
        </div>

    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-ro4ncK60IE" id="gallery2-1t">



        <div>
            <div>
                <!-- Filter -->
                <!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery2-1t" data-slide-to="0" data-toggle="modal"><img src="assets/images/FIRA2017.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">FIRA ROBOWORLDCUP 2017</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Responsive">
                                    <div href="#lb-gallery2-1t" data-slide-to="1" data-toggle="modal"><img src="assets/images/robocup17.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">ROBOCUP 2017</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Creative">
                                    <div href="#lb-gallery2-1t" data-slide-to="2" data-toggle="modal"><img src="assets/images/FIRA2018.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">FIRA ROBOWORLDCUP 2018</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery2-1t" data-slide-to="3" data-toggle="modal"><img src="assets/images/robocup18.jpeg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">ROBOCUP 2018</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery2-1t" data-slide-to="4" data-toggle="modal"><img src="assets/images/RISTEKDIKTI.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">Visit from RISTEKDIKTI</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery2-1t" data-slide-to="5" data-toggle="modal"><img src="assets/images/ism.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">Imbound Staff Mobility 2019 from ITS IO</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Responsive">
                                    <div href="#lb-gallery2-1t" data-slide-to="6" data-toggle="modal"><img src="assets/images/yatimmandiri.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">Ichiro on Yatim Mandiri 25th anniversary</span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery2-1t" data-slide-to="7" data-toggle="modal"><img src="assets/images/kri.jpg" alt="" title=""><span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7">Launching Tim KRI Regional IV</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery2-1t">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <ol class="carousel-indicators">
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" class=" active" data-slide-to="0"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="1"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="2"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="3"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="4"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="5"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="6"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery2-1t" data-slide-to="7"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img src="assets/images/FIRA2017.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/robocup17.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/FIRA2018.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/robocup18.jpeg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/RISTEKDIKTI.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/ism.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/yatimmandiri.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/kri.jpg" alt="" title=""></div>
                                </div><a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery2-1t"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control carousel-control-next"
                                    role="button" data-slide="next" href="#lb-gallery2-1t"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a>
                                <a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="mbr-section info1 cid-ro4ofpGaAb" id="info1-1u">




        <div class="container">
            <div class="row justify-content-center content-row">
                <div class="media-container-column title col-12 col-lg-7 col-md-6">

                    <h2 class="align-left mbr-bold mbr-fonts-style display-2">More of Our Moments</h2>
                </div>
                <div class="media-container-column col-12 col-lg-3 col-md-4">

                </div>
            </div>
        </div>
    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-ro4lOqjMdw" id="gallery3-1p">
        <div>
            <!-- Filter -->
            <!-- Gallery -->
            <div class="mbr-gallery-row">
                <div class="mbr-gallery-layout-default">
                    <div>
                        <div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Awesome">
                                <div href="#lb-gallery3-1p" data-slide-to="0" data-toggle="modal"><img src="assets/images/rektor.JPG" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Responsive">
                                <div href="#lb-gallery3-1p" data-slide-to="1" data-toggle="modal"><img src="assets/images/robot.jpg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Creative">
                                <div href="#lb-gallery3-1p" data-slide-to="2" data-toggle="modal"><img src="assets/images/fira2.JPG" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                <div href="#lb-gallery3-1p" data-slide-to="3" data-toggle="modal"><img src="assets/images/rbt.jpg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Awesome">
                                <div href="#lb-gallery3-1p" data-slide-to="4" data-toggle="modal"><img src="assets/images/smp.jpg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Awesome">
                                <div href="#lb-gallery3-1p" data-slide-to="5" data-toggle="modal"><img src="assets/images/3robocup .jpeg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Responsive">
                                <div href="#lb-gallery3-1p" data-slide-to="6" data-toggle="modal"><img src="assets/images/piala.jpg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                <div href="#lb-gallery3-1p" data-slide-to="7" data-toggle="modal"><img src="assets/images/fira3-864x648.jpg" alt="" title=""><span class="icon-focus"></span></div>
                            </div>
                            <!-- <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-1p" data-slide-to="8" data-toggle="modal"><img src="assets/images/kri.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-1p" data-slide-to="9" data-toggle="modal"><img src="assets/images/robotrame.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-1p" data-slide-to="10" data-toggle="modal"><img src="assets/images/piala.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>

                                <div class="mbr-gallery-item mbr-gallery-item--p0" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-1p" data-slide-to="11" data-toggle="modal"><img src="assets/images/ism.jpg" alt="" title=""><span class="icon-focus"></span></div>

                                </div> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery3-1p">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img src="assets/images/rektor.JPG" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/robot.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/fira2.JPG" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/rbt.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/smp.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/3robocup .jpeg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/piala.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/fira3-864x648.jpg" alt="" title=""></div>
                                    <!-- <div class="carousel-item"><img src="assets/images/kri.jpg" alt="" title=""></div>
                                        <div class="carousel-item"><img src="assets/images/robotrame.jpg" alt="" title=""></div>
                                        <div class="carousel-item"><img src="assets/images/piala.jpg" alt="" title=""></div>
                                        <div class="carousel-item"><img src="assets/images/ism.jpg" alt="" title=""></div> -->

                                </div><a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery3-1p"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control carousel-control-next"
                                    role="button" data-slide="next" href="#lb-gallery3-1p"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a>
                                <a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">
        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
    <script src="assets/masonry/masonry.pkgd.min.js"></script>
    <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
    <script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
    <script src="assets/theme/js/script.js"></script>
    <script src="assets/slidervideo/script.js"></script>
    <script src="assets/gallery/player.min.js"></script>
    <script src="assets/gallery/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>