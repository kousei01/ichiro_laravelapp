@extends('layouts.app')

@section('content')
<section class="mbr-fullscreen mbr-parallax-background" id="header2-1">
        <div class="mbr-overlay" style="opacity: 0.6; background-color: white;"></div>
        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div>
                    <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-2">Admin</h3>
                </div>
            </div>
        </div>
    </section>
    
@endsection