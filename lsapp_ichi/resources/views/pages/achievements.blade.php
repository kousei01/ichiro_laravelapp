{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Achievements - Ichiro ITS</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">



        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        History</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="mbr-section content5 cid-rnZHvLz4HW mbr-parallax-background" id="content5-p">



        <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);">
        </div>

        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                        Achievements</h2>



                </div>
            </div>
        </div>
    </section>

    <section class="timeline2 cid-ro05HGwSyy" id="timeline2-1c">
        <div class="container align-center">

            <h3 class="mbr-section-subtitle pb-5 mbr-fonts-style display-5">Our achievements through all competitions that we have participated in</h3>

            <div class="container timelines-container" mbri-timelines="">
                <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-pages mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2012</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2012:
                                <br> 1. 2nd Place Regional IV Champion in Surabaya <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element  separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-responsive mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2013</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2013:
                                <br> 1. 2nd Place Regional IV Champion in Surabaya 
                                <br> 2. 3rd Place National Champion in Semarang 
                                <br> 3. National Favorite Robot Team Award in Semarang <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-watch mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2014</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2014:
                                <br> 1. 2nd Place Regional IV Champion in Malang 
                                <br> 2. Best Strategy Award for Regional IV Indonesia Humanoid Soccer Robot Contest in Malang 
                                <br> 3. 2nd Place National Champion in Yogyakarta <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element  separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-shopping-basket mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2015</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2015:
                                <br> 1. Regional IV Champion in Surabaya 
                                <br> 2. 2nd Place National Champion in Yogyakarta <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-new-file mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2016</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2016: 
                                <br> 1. Regional IV Champion in Jember 
                                <br> 2. Best Strategy Award for Regional IV in Jember
                                <br> 3. 3rd Place National Champion in Surabaya 
                                <br> 4. Champion of Humanoid Robot Racing in Lombok 
                                
                                <br> <br> FIRA HUROCUP 2016: 
                                <br> 1. 2nd & 3rd Place in ALL ROUND 
                                <br> 2. 1st & 2nd Place in Marathon Category 
                                <br> 3. 1st & 2nd Place in Sprint Category 
                                <br> 4. 1st & 2nd Place in Soccer Category 
                                <br> 5. 2nd Place in the Obstacle Run Category
                                <br> 6. 3rd Place in Weight Lifting Category <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-pages mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2017</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                Indonesia Robot Contest 2017 : 
                                <br> 1. 1st Place Regional Champion in Malang 
                                <br> 2. 2nd Place National Champion in Bandung 
                                
                                <br> <br> Internasional FIRA HUROCUP 2017 : 
                                <br> 1. 1st Place in Kidsize Category - Ichiro 2 
                                <br> 2. 3rd Place in Kidsize Category 3 kidsize - Ichiro 1 
                                <br> 3. 3rd Place in Adult Size Category - Ichiro 3 
                                <br> 4. 1st Place in Kidsize Mini DRC Category - Ichiro 2 
                                <br> 5. 2nd Place in Mini DRC Adult size Category - Ichiro 3 
                                <br> 6. 1st Place in United Soccer Kidsize Category - Ichiro 2 
                                <br> 7. 2nd Place in United Soccer Kidsize Category - Ichiro 1 
                                <br> 8. 2nd Place in Weight Lifting Kidsize Category - Ichiro 1 
                                <br> 9. 2nd Place in Obstacle Run Kid Size Category - Ichiro 1 
                                <br> 10. 3rd Place in Obstacle Run Kid Size Category - Ichiro 2 
                                <br> 11. 1st Place in Sprint Kid Size Category - Ichiro 2 
                                <br> 12. 2nd Place in Sprint Adult Size Category - Ichiro 3 
                                <br> 13. 3rd Place in Marathon Adult Size Category - Ichiro 3 
                                <br> 14. 2nd Place in Obstacle Run Adult Size Category - Ichiro 3 <br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-responsive mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2018</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                ROBOCUP 2018 :
                                <br>1. 1st Place in Teen Size Humanoid Soccer League
                                <br>2. 2nd Place in Teen Size Drop-in Games
                                <br>3. 2nd Place in Teen Size Technical Challenge
                                <br>4. 3rd Place in Best Humanoid Robot

                                <br><br> FIRA ROBOWORLDCUP 2018
                                <br> 1. 1st & 2nd Place in United Soccer Kidsize Category
                                <br> 2. 1st & 3rd Place in All Round Kidsize Category
                                <br> 3. 2nd & 3rd Place in Obstacle Run Kidsize Category
                                <br> 4. 1st Place in Kidsize MiniDRC Category
                                <br> 5. 1st Place in Sprint Kidsize Category
                                <br> 6. 2nd Place in MiniDRC Adultsize Category
                                <br> 7. 2nd Place in Weight Lifting Kidsize Category
                                <br> 8. 2nd Place in Obstacle Run Adultsize Category
                                <br> 9. 2nd Place in Sprint Adultsize Category
                                <br> 10. 3rd Place in Adultsize Marathon Category
                                <br> 11. 3rd Place in All Round Adultsize Category <br> 
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row timeline-element separline">
                    <span class="iconsBackground">
                    <!-- <span class="mbri-watch mbr-iconfont"></span> -->
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2019</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                ROBOCUP 2019 :
                                <br> 1. 1st Place in Teen Size Technical Challenge
                                <br> 2. 3rd Place in Teen Size Humanoid Soccer League
                                <br> 3. 3rd Place in Kid Size Technical Challenge

                                <br><br> FIRA ROBOWORLDCUP 2019:
                                <br> 1. 1st Place in Weightlifting Adultsize Category
                                <br> 2. 1st Place in Penalty Kick Adultsize Category
                                <br> 3. 1st Place in Sprint Adultsize Category
                                <br> 4. 1st Place in Sprint Kidsize Category
                                <br> 5. 1st Place in Obstacle Run Adultsize Category
                                <br> 6. 2nd Place in Obstacle Run Kidsize Category
                                <br> 7. 2nd Place in MiniDRC Adultsize Category
                                <br> 8. 1st & 3rd Place in MiniDRC Kidsize Category
                                <br> 9. 2nd Place in Marathon Adultsize Category
                                <br> 10. 2nd Place in the All Round Adultsize Category
                                <br> 11. 3rd Place in All Round Kidsize Category
                            </p>
                        </div>
                    </div>
                </div>

                <!-- <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <span class="mbri-shopping-basket mbr-iconfont"></span>
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2020</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                COMING SOON...
                            </p>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="row timeline-element separline">
                    <span class="iconsBackground">
                    <span class="mbri-new-file mbr-iconfont"></span>
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2021</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                COMING SOON...
                            </p>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="row timeline-element reverse separline">
                    <span class="iconsBackground">
                    <span class="mbri-shopping-basket mbr-iconfont"></span>
                    </span>
                    <div class="col-xs-12 col-md-6 align-left">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2022</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                COMING SOON...
                            </p>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="row timeline-element">
                    <span class="iconsBackground">
                    <span class="mbri-new-file mbr-iconfont"></span>
                    </span>
                    <div class="col-xs-12 col-md-6 align-left ">
                        <div class="timeline-text-content">
                            <h4 class="mbr-timeline-title pb-3 mbr-fonts-style display-5">
                                2023</h4>
                            <p class="mbr-timeline-text mbr-fonts-style display-7">
                                COMING SOON...
                            </p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">
        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/theme/js/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>