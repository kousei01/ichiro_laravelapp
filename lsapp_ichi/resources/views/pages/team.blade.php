{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
        @if(count($divisions) >0)
            <ul>
                @foreach ($divisions as $division)
                    <li>{{$division}}</li>
                @endforeach
            </ul>
        @endif
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Team - Ichiro ITS</title>
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">



        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        Oprec</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="mbr-section content5 cid-rnZHvLz4HW mbr-parallax-background" id="content5-p">



        <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);">
        </div>

        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                        Team</h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Meat Our Awesome Team</h3>


                </div>
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZTVfR3L6" id="team1-12">



        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">MENTOR</h2>

            <div class="row media-row">

                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/pak tadin_.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Muhtadin, ST., MT</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mentor</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.linkedin.com/in/muhtadin-muhtadin-4ba14a47" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/pak atamimi.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Muhammad Attamimi B.Eng.,M.Eng.,Ph.D</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mentor</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZTWZLhzu" id="team1-13">
        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">TEAM LEADER</h2>

            <div class="row media-row">
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas ali.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Sulaiman Ali</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Team Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/sulaimannali/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas vian.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Oktaviansyah Purwo Bramastyo</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Team Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/vian_hahaha/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZTXtvNaQ" id="team1-14">



        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">Official Division</h2>
            <h3 class="pb-5 mbr-section-subtitle mbr-fonts-style mbr-light display-5"></h3>
            <div class="row media-row">

                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mbak assa.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Syifaul Qolby Asshakina</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Official Division Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/syifaulqlby/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mbak sinta.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Sinta Devi Listianah</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Official Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/_sinta_d/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/arsy.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Arsy Huda Fathaniard</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Sponsorship and Public Relation</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/ahufaa/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/fikar.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Dzulfikar Ats Tsauri</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Design and Branding</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/dzulfikar.at/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZTXUfwnQ" id="team1-15">



        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">
                Mechanical Division</h2>
            <h3 class="pb-5 mbr-section-subtitle mbr-fonts-style mbr-light display-5"></h3>
            <div class="row media-row">

                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas vian.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Oktaviansyah Purwo Bramastyo</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mechanical Division Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/vian_hahaha/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas even.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Even Clarence</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mechanical Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/evenclarence/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/amik.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Amik Rafly Azmi Ulya</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mechanical Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/velonezh/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/axel.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Axel Dawne</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Mechanical Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/axeldawne_/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZTYkr0gg" id="team1-16">



        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">Electrical Division</h2>
            <h3 class="pb-5 mbr-section-subtitle mbr-fonts-style mbr-light display-5"></h3>
            <div class="row media-row">

                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas nando.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Ahmad Hernando Pradanatta Putra</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Electrical Division Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/ahmadhernando/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/bonar.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                David Bonar</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Electrical Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/davidbonarr/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/arief.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Mohammad Arief Darmawan</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Electrical Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>

    <section class="team1 cid-rnZZALz1xt" id="team1-19">



        <div class="container align-center">
            <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2">Programming Division</h2>
            <h3 class="pb-5 mbr-section-subtitle mbr-fonts-style mbr-light display-5"></h3>
            <div class="row media-row">

                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas reza.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                M Reza Ar Razi</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Leader</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/rezaarrazi/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/tommy-660x991.jpg" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Tommy Pratama</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/tommyypra/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas ari.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                I Made Pande Ari Wijaya</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/ari_wijaya82/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/mas alfi.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Muhammad Alfi Maulana Fikri</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/alf_maulana/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/daffa.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                R. Dafa Berlian Denmar</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com/dafa.berlian" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/dafa.berlian/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/ega.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Segara Bhagas Dagsapurwa</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <!-- <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.instagram.com/cerelyn_24/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div>
                {{-- <div class="team-item col-lg-3 col-md-6">
                    <div class="item-image">
                        <img src="assets/images/zakiya.png" alt="" title="">
                    </div>
                    <div class="item-caption py-3">
                        <div class="item-name px-2">
                            <p class="mbr-fonts-style display-5">
                                Zakiya Azizah Cahyaningtyas</p>
                        </div>
                        <div class="item-role px-2">
                            <p>Programming Division Member</p>
                        </div>
                        <div class="item-social pt-2">
                            <!-- <a href="https://twitter.com" target="_blank">
                                <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                            <a href="https://www.facebook.com" target="_blank">
                                <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://plus.google.com" target="_blank">
                                <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.linkedin.com" target="_blank">
                                <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <a href="https://www.instagram.com/zkyazz/" target="_blank">
                                <span class="p-1 socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                            <!-- <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                <span class="p-1 socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a> -->
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">





        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <!-- <div class="soc-item">
                                <a href="https://www.facebook.com" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://instagram.com/ichiro.its" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <!-- <div class="soc-item">
                                <a href="https://www.behance.net" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/theme/js/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>