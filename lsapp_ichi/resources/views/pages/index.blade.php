{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Ichiro ITS</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/as-pie-progress/css/progress.min.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/gallery/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        History</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="cid-qTkA127IK8 mbr-fullscreen mbr-parallax-background" id="header2-1">
        <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);"></div>

        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">ICHIRO ITS ROBOTICS TEAM</h1>
                    <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-2">2018 Teen Size Robocup Champions</h3>
                    <p class="mbr-text pb-3 mbr-fonts-style display-5">We Develop Humanoid and Soccer Robot<br></p>

                </div>
            </div>
        </div>
        <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
            <a href="#next">
                <i class="mbri-down mbr-iconfont"></i>
            </a>
        </div>
    </section>

    <section class="clients cid-rnVoDpNM5V mbr-parallax-background" data-interval="false" id="clients-d">


        <div class="mbr-overlay" style="opacity: 0.9; background-color: rgb(255, 255, 255);">
        </div>
        <div class="container mb-5">
            <div class="media-container-row">
                <div class="col-12 align-center">
                    <h2 class="mbr-section-title pb-3 mbr-fonts-style display-2">Sponsored By</h2>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="carousel slide" role="listbox" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="6000">
                <div class="carousel-inner" data-visible="6">

                    <div class="carousel-item ">
                        <div class="media-container-row">
                            <div class="col-md-12">
                                <img src="assets/images/sponsor semua.png" style="width: 100%;">
                                <!-- <div class="wrap-img "> -->
                                <!-- <img src="assets/images/logobppt.png" class="img-responsive clients-img">
                                    <img src="assets/images/logotraktor.png" class="img-responsive clients-img"> -->

                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- <div class="carousel-item ">
                        <div class="media-container-row">
                            <div class="col-md-12">
                                <div class="wrap-img ">
                                    <img src="assets/images/resized_sponsor.png" class="img-responsive clients-img">
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="carousel-item ">
                        <div class="media-container-row">
                            <div class="col-md-12">
                                <div class="wrap-img ">
                                    <img src="assets/images/3.png" class="img-responsive clients-img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <div class="media-container-row">
                            <div class="col-md-12">
                                <div class="wrap-img ">
                                    <img src="assets/images/4.png" class="img-responsive clients-img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <div class="media-container-row">
                            <div class="col-md-12">
                                <div class="wrap-img ">
                                    <img src="assets/images/5.png" class="img-responsive clients-img">
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- <div class="carousel-controls">
                    <a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev">
                        <span aria-hidden="true" class="mbri-left mbr-iconfont"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next">
                        <span aria-hidden="true" class="mbri-right mbr-iconfont"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> -->
            </div>
        </div>
    </section>

    <section class="header1 cid-rnVmC6oSri" id="header16-4">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">Intro
                    </h1>

                    <p class="mbr-text pb-3 mbr-fonts-style display-5">
                    Ichiro Team is a Soccer Robot Team from the Sepuluh Nopember Institute of Technology (ITS) under the patronage of ITS Robotics student activity unit. Ichiro team's main focus is to develop a humanoid soccer robot. 
                    This will be done continuosly until these robots have the ability to match humans. The Ichiro team has participated in the national robot contests since 2010. Until this year, Ichiro team participates actively and has won many robot contests, both nationally and internationally. 
                    Currently, Ichiro Team has five humanoid robots that have the ability to kick, dribble, and are able to work together in teams to defend or attack. All robot movements can be run automatically and independently.
                    </p>
                    <!-- <div class="mbr-section-btn"><a class="btn btn-md btn-black-outline display-4" href="/about" target="_blank">LEARN MORE</a></div> -->
                </div>
            </div>
        </div>

    </section>

    <section class="carousel slide cid-rnVnCkuwvb" data-interval="false" id="slider1-8">

        <div class="full-screen">
            <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="7000">
                <ol class="carousel-indicators">
                    <li data-app-prevent-settings="" data-target="#slider1-8" class=" active" data-slide-to="0"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-8" data-slide-to="1"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-8" data-slide-to="2"></li>
                    <li data-app-prevent-settings="" data-target="#slider1-8" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url(assets/images/5robocup-ichiro1-1600x1200.jpg);">
                        <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay"></div><img src="assets/images/5robocup-ichiro1-1600x1200.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-right">
                                        <h2 class="mbr-fonts-style display-1">WE ARE THE CHAMPION!!!</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">ICHIRO - ITS Champion in RoboCup 2018</p>
                                        <!-- <div class="mbr-section-btn" buttons="0"> <a class="btn  btn-white-outline display-4" href="/" target="_blank">Learn More</a></div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="https://www.youtube.com/watch?v=s4eYPCe2Tfc">
                        <div class="mbr-overlay"></div>
                        <div class="container container-slide">
                            <div class="image_wrapper"><img src="assets/images/background2.jpg" style="opacity: 0;">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left">
                                        <h2 class="mbr-fonts-style display-1">Aftermovie ROBOCUP 2019</h2>
                                        {{-- <p class="lead mbr-text mbr-fonts-style display-5">A short movie about our journey through 2018</p> --}}
                                        <div class="mbr-section-btn" buttons="0"> <a class="btn  btn-white-outline display-4" href="https://www.youtube.com/watch?v=s4eYPCe2Tfc" target="_blank">WATCH THE FULL VERSION</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="https://www.youtube.com/watch?v=GMEmFvv0SOw">
                        <div class="mbr-overlay"></div>
                        <div class="container container-slide">
                            <div class="image_wrapper"><img src="assets/images/background2.jpg" style="opacity: 0;">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left">
                                        <h2 class="mbr-fonts-style display-1">Aftermovie FIRA Roboworldcup 2019</h2>
                                        {{-- <p class="lead mbr-text mbr-fonts-style display-5">A short movie about our journey through 2018</p> --}}
                                        <div class="mbr-section-btn" buttons="0"> <a class="btn  btn-white-outline display-4" href="https://www.youtube.com/watch?v=GMEmFvv0SOw" target="_blank">WATCH THE FULL VERSION</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="https://www.youtube.com/watch?v=tkXSHAZW3Xg">
                        <div class="mbr-overlay"></div>
                        <div class="container container-slide">
                            <div class="image_wrapper"><img src="assets/images/background2.jpg" style="opacity: 0;">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left">
                                        <h2 class="mbr-fonts-style display-1">2018 : AFTERMOVIE</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">A short movie about our journey through 2018</p>
                                        <div class="mbr-section-btn" buttons="0"> <a class="btn  btn-white-outline display-4" href="https://www.youtube.com/watch?v=tkXSHAZW3Xg" target="_blank">WATCH THE FULL VERSION</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(assets/images/img-20190213-wa0000-1280x853.jpg);"> -->
                        <!-- <div class="container container-slide">
                            <div class="image_wrapper">
                                <div class="mbr-overlay" style="opacity: 0.5;"></div><img src="assets/images/img-20190213-wa0000-1280x853.jpg">
                                <div class="carousel-caption justify-content-center">
                                    <div class="col-10 align-left">
                                        <h2 class="mbr-fonts-style display-1">TEAM MEMBER</h2>
                                        <p class="lead mbr-text mbr-fonts-style display-5">Meet our team member!</p>
                                        <div class="mbr-section-btn" buttons="0"> <a class="btn  btn-white-outline display-4" href="/" target="_blank">Learn More</a></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- </div> -->
                </div><a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-8"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a>
                <a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-8"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a>
            </div>
        </div>
        
    </section>
    
    <section class="countdown1 cid-ropobsqy4m mbr-parallax-background" id="countdown1-0">
        <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
        </div>
        <div class="container ">
            <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">
                <br>Countdown to RoboCup 2020</h2>
            <h3 class="mbr-section-subtitle align-center mbr-fonts-style display-5">23 - 29 June 2020 in Bordeaux, France</h3>

        </div>
        <div class="container countdown-cont align-center">
            <div class="daysCountdown" title="Days"></div>
            <div class="hoursCountdown" title="Hours"></div>
            <div class="minutesCountdown" title="Minutes"></div>
            <div class="secondsCountdown" title="Seconds"></div>
            <div class="countdown pt-5 mt-2" data-due-date="2020/06/23">
                <!--tahun/bulan/tanggal-->>
            </div>
        </div>
    </section>

    {{-- news --}}
    @if(count($posts) > 0)
        <section class="features18 popup-btn-cards cid-rnVnTquo9z" id="features18-a">
            <div class="container">
                <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">
                    News</h2>
                <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">
                    Ichiro ITS News, activities, and more</h3>
                <div class="media-container-row pt-5 ">
                    {{-- post --}}
                    @foreach ($posts as $post)
                        <div class="card p-3 col-12 col-md-6 col-lg-4">
                            <div class="card-wrapper ">
                                <div class="card-img">
                                    <div class="mbr-overlay"></div>
                                    <div class="mbr-section-btn text-center"><a href="/news/{{$post->id}}" class="btn btn-primary display-4">Learn More</a></div>
                                    <img src="/storage/cover_images/{{$post->cover_image}}" alt="Mobirise" title="">
                                </div>
                                <div class="card-box">
                                    <h4 class="card-title mbr-fonts-style display-7">{{$post->title}}</h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <div class="mbr-overlay"></div>
                                <div class="mbr-section-btn text-center"><a href="/news/26" class="btn btn-primary display-4">Learn More</a></div>
                                <img src="assets/images/menangfira-825x550.jpg" alt="Mobirise" title="">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7">Ichiro ITS, Jawara FIRA Hurocup 2016</h4>

                            </div>
                        </div>
                    </div>

                    <div class="card p-3 col-12 col-md-6 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <div class="mbr-overlay"></div>
                                <div class="mbr-section-btn text-center"><a href="/news/25" class="btn btn-primary display-4">Learn More</a></div>
                                <img src="assets/images/fira1-860x678.jpg" alt="Mobirise" title="">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-7">Ichiro ITS Juara Umum Fira Hurocup 2017</h4>

                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </section>
    @else
        <section class="features18 popup-btn-cards cid-rnVnTquo9z" id="features18-a">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-10 align-center">
                        <!-- <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1"><a href="/news">News</a></h1> -->
                        <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2"><a href="/news">News</a></h2>
                        <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">
                        Ichiro ITS News, activities, and more</h3>

                        <p class="mbr-text pb-3 mbr-fonts-style display-5">COMING SOON...</p>
                    </div>
                </div>

                {{-- <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2"><a href="/news">
                    News</a></h2>
                <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">
                    Ichiro ITS News, activities, and more</h3>
                <br>
                <!-- <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">
                    COMING SOON...</h3><br> -->
                <div class="col-md-10 align-center">
                    <p class="mbr-text pb-3 mbr-fonts-style display-5">COMING SOON...</p>
                </div> --}}
            </div>
        </section>
    @endif

    <!-- <section class="mbr-section info3 cid-ro07KZyVNT mbr-parallax-background" id="info3-1d">
        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(35, 35, 35);">
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="media-container-column title col-12 col-md-10">
                    <h2 class="align-left mbr-bold mbr-white pb-3 mbr-fonts-style display-2">
                        History</h2>
                    <h3 class="mbr-section-subtitle align-left mbr-light mbr-white pb-3 mbr-fonts-style display-5">A Short Story of Our History</h3>

                    <div class="mbr-section-btn align-left py-4"><a class="btn btn-white-outline display-4" href="/oprec">Check It Out !!!</a></div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- <section class="progress-bars3 cid-rnVoaiqEMb" id="progress-bars3-b">
        <div class="container">
            <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">
                <a href="/achievements" class="text-primary"> </a>
            </h2>

            <h3 class="mbr-section-subtitle mbr-fonts-style display-5"> </h3>

            <h3 class="mbr-section-subtitle mbr-fonts-style display-5">
                Our achievements through all competitions that we have participated in</h3>

            <div class="media-container-row pt-5 mt-2">
                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress1" role="progressbar" data-goal="54">
                            <p class="pie_progress__number mbr-fonts-style display-5"></p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">first position</h4>
                    </div>
                </div>

                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress2" role="progressbar" data-goal="60">
                            <p class="pie_progress__number mbr-fonts-style display-5">70%</p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">second position</h4>
                    </div>
                </div>

                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress3" role="progressbar" data-goal="70">
                            <p class="pie_progress__number mbr-fonts-style display-5"></p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">
                            third position</h4>
                    </div>
                </div>

                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress4" role="progressbar" data-goal="80">
                            <p class="pie_progress__number mbr-fonts-style display-5"></p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">
                            finalist</h4>
                    </div>
                </div>

                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress5" role="progressbar" data-goal="90">
                            <p class="pie_progress__number mbr-fonts-style display-5"></p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">
                            semi-finalist</h4>
                    </div>
                </div>

                <div class="card p-3 align-center">
                    <div class="wrap">
                        <div class="pie_progress progress6" role="progressbar" data-goal="100">
                            <p class="pie_progress__number mbr-fonts-style display-5"></p>
                        </div>
                    </div>
                    <div class="mbr-crt-title pt-3">
                        <h4 class="card-title py-2 mbr-fonts-style display-5">
                            participant</h4>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section class="mbr-section info3 cid-ro0e8y4szi mbr-parallax-background" id="info3-1e">
        <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(35, 35, 35);">
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="media-container-column title col-12 col-md-10">
                    <h2 class="align-left mbr-bold mbr-white pb-3 mbr-fonts-style display-2">
                        Team</h2>
                    <h3 class="mbr-section-subtitle align-left mbr-light mbr-white pb-3 mbr-fonts-style display-5">Meet Our Awesome Team!!!</h3>

                    <div class="mbr-section-btn align-left py-4"><a class="btn btn-white-outline display-4" href="/team">MORE</a></div>
                </div>
            </div>
        </div>
    </section>

    <section class="features15 cid-rnVDijxft9" id="features15-k">




        <div class="container">
            <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2"><a href="/robots">
            Robots</a></h2>
            <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style">
                More about our robots</h3>

            <div class="media-container-row container pt-5 mt-2">
                <div class="col-12 col-md-6 mb-4 col-lg-3">
                    <div class="card flip-card p-5 align-center">
                        <div class="card-front card_cont">
                            <img src="assets/images/robot_besar.jpeg" alt="Mobirise">
                        </div>
                        <div class="card_back card_cont">
                            <h4 class="card-title display-5 py-2 mbr-fonts-style">
                                <a href="/robots#features11-23">
                                    <br><br><br>Teen Size Humanoid Robot
                                </a>
                            </h4>
                            <p class="mbr-text mbr-fonts-style display-7"></p>
                            <!-- <p><a href="/" target="_blank">
                            One Tech.
                        </a></p> -->
                            <p></p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 mb-4 col-lg-3">

                    <div class="card flip-card p-5 align-center">
                        <div class="card-front card_cont">
                            <img src="assets/images/ROBOT KECIL.png" alt="Mobirise">
                        </div>
                        <div class="card_back card_cont">
                            <h4 class="card-title py-2 mbr-fonts-style display-5">
                                <a href="/robots#features11-24">
                                    <br><br><br>Kid Size Humanoid Robot
                                </a>
                            </h4>
                            <!-- <p class="mbr-text mbr-fonts-style display-7"><a href="/" target="_blank">
                            Two Tech.
                        </a></p> -->
                        </div>
                    </div>
                </div>

                <!-- <div class="col-12 col-md-6 mb-4 col-lg-3">
                    <div class="card flip-card p-5 align-center">
                        <div class="card-front card_cont">
                            <img src="assets/images/background3.jpg" alt="Mobirise">
                        </div>
                        <div class="card_back card_cont">
                            <h4 class="card-title py-2 mbr-fonts-style display-5"><a href="/" target="_blank">
                            Three
                        </a></h4>
                            <p class="mbr-text mbr-fonts-style display-7"><a href="/" target="_blank">
                            Three Tech.
                        </a></p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 mb-4 col-lg-3">
                    <div class="card flip-card p-5 align-center">
                        <div class="card-front card_cont">
                            <img src="assets/images/background4.jpg" alt="Mobirise">
                        </div>
                        <div class="card_back card_cont">
                            <h4 class="card-title py-2 mbr-fonts-style display-5"><a href="/" target="_blank">
                            Four 
                        </a></h4>
                            <p class="mbr-text mbr-fonts-style display-7"><a href="/" target="_blank">
                            Four Tech.
                        </a></p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-rnVEJ89oso" id="gallery3-m">
        <div>
            <div>
                <!-- Filter -->
                <!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery3-m" data-slide-to="0" data-toggle="modal"><img src="assets/images/rektor.JPG" alt="" title=""> <span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                                    <div href="#lb-gallery3-m" data-slide-to="1" data-toggle="modal"><img src="assets/images/robot3.JPG" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Creative">
                                    <div href="#lb-gallery3-m" data-slide-to="2" data-toggle="modal"><img src="assets/images/rbt.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-m" data-slide-to="3" data-toggle="modal"><img src="assets/images/img-20190213-wa0000-1280x853.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery3-m" data-slide-to="4" data-toggle="modal"><img src="assets/images/fira1.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery3-m" data-slide-to="5" data-toggle="modal"><img src="assets/images/FIRA2018.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                                    <div href="#lb-gallery3-m" data-slide-to="6" data-toggle="modal"><img src="assets/images/FIRA2017.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery3-m" data-slide-to="7" data-toggle="modal"><img src="assets/images/fira3-864x648.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery3-m">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img src="assets/images/rektor.JPG" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/robot3.JPG" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/rbt.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/img-20190213-wa0000-1280x853.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/fira1.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/FIRA2018.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/FIRA2017.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="assets/images/fira3-864x648.jpg" alt="" title=""></div>
                                </div><a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery3-m"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control carousel-control-next"
                                    role="button" data-slide="next" href="#lb-gallery3-m"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a>
                                <a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">
        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232<br></p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com/" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com/" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/c/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com/" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net/" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/countdown/jquery.countdown.min.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
    <script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
    <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
    <script src="assets/masonry/masonry.pkgd.min.js"></script>
    <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="assets/mbr-popup-btns/mbr-popup-btns.js"></script>
    <script src="assets/as-pie-progress/jquery-as-pie-progress.min.js"></script>
    <script src="assets/mbr-flip-card/mbr-flip-card.js"></script>
    <script src="assets/mbr-clients-slider/mbr-clients-slider.js"></script>
    <script src="assets/theme/js/script.js"></script>
    <script src="assets/slidervideo/script.js"></script>
    <script src="assets/gallery/player.min.js"></script>
    <script src="assets/gallery/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>