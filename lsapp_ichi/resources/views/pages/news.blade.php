{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>News - Ichiro ITS</title>
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">



</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">



        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        History</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="mbr-section content5 cid-rnZHvLz4HW mbr-parallax-background" id="content5-p">



        <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);">
        </div>

        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                        News</h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Ichiro ITS News, activities, and more</h3>


                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section content7 cid-rnZJ427bsK" id="content7-v">
        <div class="container">
            <div class="media-container-row">
                <div class="col-12 col-md-12">
                    <div class="media-container-row">
                        <div class="media-content">
                            <div class="mbr-section-text">
                                <p class="mbr-text align-right mb-0 mbr-fonts-style display-7"></p>
                                <h3><strong><br><br>Sempat Pincang, Tim Ichiro 2 Kantongi Lima Piala</strong></h3><br>
                                <p>"Pada saat latihan di lab kami, robot Ichiro 2 dapat berjalan dan lari sangat baik. Tapi saat dicoba di lokasi jadi pincang, lalu coba kami perbaiki namun tetap saja pincang," ungkap Reza Ar Razi, Ketua Ichiro.
                                    <br>
                                    <br>Menurut Reza, Kemungkinan ada satu servo yang bermasalah, namun tim tidak mengetahui servo mana yang rusak. "Sudah kami ganti tiga servo, tapi tetap saja timpang kakinya. Hingga hari terakhir, keadaan robot tetap
                                    seperti itu," cerita mahasiswa Departemen Teknik Informatika itu pada ITS Online.
                                    <br>
                                    <br>Servo sendiri merupakan sistem kontrol yang mengubah gerak mekanis kecil menjadi satu yang membutuhkan kekuatan yang jauh lebih besar.
                                    <br>
                                    <br>Hal tersebut tentunya sangat berpengaruh pada kompetisi yang membutuhkan jalan cepat seperti sprint, "Kita sempat khawatir karena saat itu final sprint. Lalu kami coba akali pada programnya agar tidak terlalu miring
                                    jalannya," ungkap Reza.
                                    <br>
                                    <br>Namun karena hambatan ini, Ichiro 2 hanya berhasil melakukan sprint dalam waktu 26 detik. Sedangkan tahun lalu, Ichiro berhasil dengan waktu 21 detik dan menjadi rekor dunia. "walaupun begitu, tetap Alhamdulillah
                                    kami bisa mendapat juara satu pada sprint, karena rata-rata yang lain hanya mencapai 29 detik," tutur Reza lega.
                                    <br>
                                    <br>Walaupun melalui berbagai hambatan, Ichiro 2 dapat mengantongi lima piala. Diantaranya, Juara satu mini DRC kidsize, Juara satu united soccer kidsize, Juara tiga halang rintang kidsize, dan Juara satu sprint kid
                                    size yang mengantarkannya meraih Juara umum kidsize. (id)<strong><br></strong></p>
                                <p><br></p>
                                <p></p>
                            </div>
                        </div>
                        <div class="mbr-figure" style="width: 100%;">
                            <img src="assets/images/fira3-864x648.jpg" alt="Mobirise" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section content7 cid-rnZN6wmxrZ" id="content7-x">



        <div class="container">
            <div class="media-container-row">
                <div class="col-12 col-md-12">
                    <div class="media-container-row">
                        <div class="media-content">
                            <div class="mbr-section-text">
                                <h3><strong><br><br>Ichiro ITS, Jawara FIRA Hurocup 2016</strong></h3>
                                <p class="mbr-text align-right mb-0 mbr-fonts-style display-7"><br>Kompetisi lima hari, sejak Rabu (14/12) pada ajang robot internasional ini, nampaknya berbuah manis bagi tim Ichiro. Semangat yang membara serta doa yang tak kunjung usai, menghantarkan tim Ichiro atas peraihan tiga
                                    medali emas dan satu medali perak.
                                    <br>
                                    <br>Adapun perolehan juaranya adalah juara 1 kategori marathon, juara 1 kategori sprint, juara 1 kategori soccer dan juara 2 kategori halang rintang. Tak hanya itu, dari sekian banyak juara yang diraih dengan total
                                    nilai 38, robot Ichiro Satu pun memperoleh tambahan penghargaan sebagai Juara Umum ketiga.
                                    <br>
                                    <br>Berbekal pesan Rektor ITS, Prof Ir Joni Hermana MSc Es PhD sebelum keberangkatan tim robot ITS, yakni untuk selalu menjaga nama baik bangsa dan negara, tim Ichiro pun membuktikannya. Robot Ichiro Dua tak mau kalah,
                                    ia juga mengantongi tiga medali perak dan satu medali perunggu.
                                    <br>
                                    <br>Perolehan juara oleh Ichiro Dua ini adalah juara 2 kategori marathon, juara 2 kategori sprint, juara 2 kategori soccer dan juara 3 kategori weight lifting. Atas pencapaiannya tersebut, total nilai yang diraih lebih
                                    tinggi, yaitu 40 dan Ichiro Dua dinobatkan sebagai Juara Umum kedua.
                                    <br>
                                    <br>Capaian ini tak lepas dari dukungan dosen pembimbing tim Ichiro, Muhtadin ST MT, pendamping tim Ichiro, Ir Rudhy Ivan Noor, pengurus IKOMA ITS bagian Pendidikan dan IPTEK, keluarga besar ITS dan Kedutaan Besar Republik
                                    Indonesia (KBRI) di Beijing.
                                    <br>
                                    <br>Sebagai dosen pembimbing, Muhtadin merasa puas atas apa yang telah dicapai oleh anak didiknya. "Alhamdulillah, capaian ini sangat memuaskan. Kami bersyukur dapat mengharumkan nama Indonesia dan ITS, terimakasih
                                    atas dukungan yang kuat dari berbagai pihak," ungkap Muhtadin dengan penuh rasa bangga. (mir/riz)</p>
                            </div>
                        </div>
                        <div class="mbr-figure" style="width: 100%;">
                            <img src="assets/images/menangfira-825x550.jpg" alt="Mobirise" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section content7 cid-rnZN7gMitu" id="content7-y">



        <div class="container">
            <div class="media-container-row">
                <div class="col-12 col-md-12">
                    <div class="media-container-row">
                        <div class="media-content">
                            <div class="mbr-section-text">
                                <h3><strong><br><br>Ichiro ITS Juara Umum Fira Hurocup 2017</strong></h3>
                                <p class="mbr-text align-right mb-0 mbr-fonts-style display-7"><br>Ichiro ITS kembali mengulang sejarahnya sebagai juara umum Fira Hurocup setelah tahun lalu mampu menggondol sepuluh penghargaan. Tahun ini, National Kaohsiung First University of Science and Technology yang menjadi
                                    saksi bisu kemenangan Ichiro ITS.
                                    <br>
                                    <br>Berlangsung sejak Rabu, (23/8) poin demi poin berhasil dikumpulkan oleh tim Ichiro. ITS Mengirimkan tiga tim yang terdiri dari tim Ichiro 1, 2 dan 3. Ketiganya sukses memuncaki posisi tertinggi dari setiap kategori
                                    yang ada.
                                    <br>
                                    <br>Tim Ichiro 1 mendapatkan juara dua united soccer kategori kidsize, juara dua weight lifting kidsize, dan juara dua dalam halang rintang kidsize ketiga penghargaan tersebut embuat Ichiro 1 menjadi Juara umum peringkat
                                    tiga kidsize.
                                    <br>
                                    <br>Tim Ichiro 2 pun tak mau kalah. Juara umum kidsize berhasil didapatkannya, kemudian juara satu mini DRC kidsize, juara satu united soccer kidsize, juara tiga halang rintang kid size, dan juara satu sprint kid size.
                                    <br>
                                    <br>Juara umum peringkat tiga adult size diraih oleh ITS melalui Ichiro 3. Robot setinggi 88 sentimeter itu berhasil menjadi juara dua mini DRC adult size, juara dua sprint adult size, juara tiga maraton Aadult size,
                                    serta juara dua halang rintang adult size.
                                    <br>
                                    <br>Reza Ar Razi, ketua tim mengungkapkan bahwa kali ini, prestasi Ichiro di Fira Horocup meningkat banyak. "kalau tahun lalu kita bawa sepuluh penghargaan. Kali ini ada empat belas penghargaan," ungkap Reza
                                    <br>
                                    <br>Tak hanya itu, tim Ichiro juga menambah kompetisi yg diikuti. "Tahun ini kita menambah kompetisi untuk mini DRC, pinalty kick, dan spartan race," tambahnya.
                                    <br>
                                    <br>Direktur Kemahasiswaan Direktorat Jenderal Pembelajaran dan Kemahasiswaan (Ditjen Belmawa) Dr Didin Waidin menyatakan kebanggaannya akan kemenangan yang diraih oleh Tim Ichiro ITS. "Ini makin memperkuat kepercayaan
                                    diri anak-anak bangsa dan optimis bahwa bangsa ini adalah bangsa besar yang siap memenangkan persaingan di masa yang akan datang. Terima kasih ITS. Hidup mahasiswa ITS! Hidup mahasiswa Indonesia!" ujarnya.
                                    <br>
                                    <br>Senada dengan Didin, dosen pembimbing tim Ichiro, Muhtadin ST MT, juga menyatakan rasa syukurnya. "Kami mengharapkan semangat ini menular ke tim-tim robot yang lain sehingga nama Indonesia menjadi besar dalam bidang
                                    Robotika. Kami juga mengharapkan ada sinergi antara tim-tim robot yang dimediasi oleh kementerian sehingga percepatan penguasaan teknologi robot dapat dicapai, dan semakin banyak tim-tim robot yang hebat," tuturnya.
                                    (id/van)
                                    <strong><br></strong></p>
                            </div>
                        </div>
                        <div class="mbr-figure" style="width: 100%;">
                            <img src="assets/images/fira1-860x678.jpg" alt="Mobirise" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">





        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.instagram.com/ichiro.its/" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/theme/js/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>