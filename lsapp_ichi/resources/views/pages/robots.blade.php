{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{config('app.name'), 'Ichiro LaravelApp'}}</title>
    </head>
    <body>
        <h1>{{$title}}</h1>
    </body>
</html> --}}

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/ichiro.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Robots - Ichiro ITS</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
</head>

<body>
    <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="/">
                         <img src="assets/images/white-text-169x60.png" alt="Ichiro" title="Ichiro ITS" style="height: 3.8rem;">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/news">News</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/team">Team
                        </a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/achievements">
                        Achievements</a></li>
                    <!-- <li class="nav-item"><a class="nav-link link text-white display-4" href="/oprec">
                        History</a></li> -->
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/robots">
                        
                        Robots</a></li>
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="/gallery">
                        Gallery</a></li>
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="/about">
                        
                        About Us
                    </a>
                    </li>
                </ul>

            </div>
        </nav>
    </section>

    <section class="engine">
        <a href="#"></a>
    </section>
    <section class="mbr-section content5 cid-rnZHvLz4HW mbr-parallax-background" id="content5-p">
        <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);">
        </div>

        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                        Robots</h2>
                    <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Check Out Some of Our Robots</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="features11 cid-roPUEd73BS" id="features11-23">
        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 60%;">
                        <img src="assets/images/robot_besar.jpeg" alt="Mobirise" title="">
                    </div>
                    <div class=" align-left aside-content">
                        <h2 class="mbr-title pt-2 mbr-fonts-style display-2"><br><br>Teen Size Humanoid Robot</h2>
                        <div class="mbr-section-text">
                            <p class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-7">Name : Ithaaro
                                <br>Height &nbsp; : 84 cm
                                <br>Weight &nbsp; : 7.2 kg
                                <br>Number of Robots : 2 Robots
                                <br>Walking Speed : 22 cm/s
                                <br>Motors :
                                <br>&nbsp; &nbsp; &nbsp;20 DOF
                                <br>&nbsp; &nbsp; &nbsp;Neck : 2 motors Dynamixel MX-28
                                <br>&nbsp; &nbsp; &nbsp;Arm : 3 motors Dynamixel MX-64 (x2)
                                <br>&nbsp; &nbsp; &nbsp;Leg : 6 motors Dynamixel MX-106 (x2)
                                <br>
                                <br>Sensors :<br>&nbsp; &nbsp; &nbsp;IMU : MPU6050
                                <br>&nbsp; &nbsp; &nbsp;Camera : Logitech C922<br><br>Computing Unit :<br>&nbsp; &nbsp; &nbsp;Main Controller : Intel NUC 7
                                <br>&nbsp; &nbsp; &nbsp;Sub Controller : CM-740, Arduino Nano<br><br>Operating System :&nbsp;Linux Ubuntu 14.04 LTS<br><br><br></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features11 cid-roPWYvsIPc" id="features11-24">
        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 60%;">
                        <img src="assets/images/ROBOT KECIL.png" alt="Mobirise" title="">
                    </div>
                    <div class=" align-left aside-content">
                        <h2 class="mbr-title pt-2 mbr-fonts-style display-2"><br><br>Kid Size Humanoid Robot</h2>
                        <div class="mbr-section-text">
                            <p class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-7">Name : Arata&nbsp;<br>Height : 58 cm
                                <br>Weight : 4.3 kg
                                <br>Number of Robots : 4 Robots
                                <br>Walking Speed : 25 cm/s
                                <br>Motors :
                                <br>&nbsp; &nbsp; &nbsp;20 DOF
                                <br>&nbsp; &nbsp; &nbsp;Upper Body : 8 motors Dynamixel MX-28
                                <br>&nbsp; &nbsp; &nbsp;Lower Body : 12 motors Dynamixel MX-64<br>
                                <br>Sensors :
                                <br>&nbsp; &nbsp; &nbsp;IMU : MPU6050
                                <br>&nbsp; &nbsp; &nbsp;Camera : Logitech C922
                                <br>
                                <br>Computing Unit :
                                <br>&nbsp; &nbsp; &nbsp;Main Controller : Intel NUC 7
                                <br>&nbsp; &nbsp; &nbsp;Sub Controller : CM-740, Arduino Nano
                                <br>
                                <br>Operating System : Linux Ubuntu 14.04 LTS
                                <br>
                                <br></p>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rnVFDMIqyT mbr-reveal" id="footer2-n">





        <div class="container">
            <div class="media-container-row content mbr-white">
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Address</strong>
                        <br>
                        <br>Gedung Robotika ITS
                        <br>ITS Campus, Sukolilo, Surabaya, Jawa Timur, Indonesia<br>
                        <br>
                        <br><strong>Contacts</strong>
                        <br>
                        <br>Email: ichiro.its@gmail.com <br>Phone: +6282143105232</p>
                </div>
                <div class="col-12 col-md-3 mbr-fonts-style display-7">
                    <p class="mbr-text">
                        <strong>Links</strong>
                        <br>
                        <br><a class="text-warning" href="https://its.ac.id" target="_blank">Institut Teknologi Sepuluh Nopember</a>&nbsp;<br><br>
                        <br><strong>Feedback</strong>
                        <br>
                        <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.677815783217!2d112.79528841374074!3d-7.277452673533937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa11b35fed03%3A0x9c97f605616e686b!2sGedung+Robotika+ITS!5e0!3m2!1sid!2sid!4v1555662849717!5m2!1sid!2sid"
                            allowfullscreen=""></iframe></div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2019 Ichiro ITS - All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <!-- <div class="soc-item">
                                <a href="https://twitter.com" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                            <div class="soc-item">
                                <a href="https://www.youtube.com/ICHIROITS" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.instagram.com/ichiro.its" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <!-- <div class="soc-item">
                                <a href="https://plus.google.com" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/web/assets/jquery/jquery.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/tether/tether.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/dropdown/js/script.min.js"></script>
    <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="assets/parallax/jarallax.min.js"></script>
    <script src="assets/smoothscroll/smooth-scroll.js"></script>
    <script src="assets/theme/js/script.js"></script>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
</body>

</html>